# Chilean Tomography - CSN version - 2019

This repository contains the velocity models built for the routine processing of the _Centro Sismológico Nacional_ (CSN, Universidad de Chile, http://www.sismologia.cl).

Contact: 
- Bertrand Potin, _DGF, University of Chile_ (bertrand.potin@uchile.cl)
- Sergio Barrientos, _CSN, University of Chile_ (sbarrien@csn.uchile.cl)
- Sergio Ruiz, _DGF, University of Chile_ (sruiz@uchile.cl)

![seismicity_and_box_map](./wd/carte_box_sta_sismo.jpg){width=30%}

_Coverage of Chile by the CSN tomography models and relocated CSN catalogue, from 1982 to 2020. Dots represent earthquakes, dot colors indicate depth. Colored boxes represent the 6 velocity model coverage._

## How to cite this material

### material doi

https://doi.org/10.5281/zenodo.13146424

### Related article

Potin, B., S. Ruiz, F. Aden-Antoniow, R. Madariaga, and S. Barrientos (2024). A Revised Chilean Seismic Catalog from 1982 to Mid-2020, _Seismol. Res. Lett._. https://doi.org/10.1785/0220240047

## Velocity models

6 velocity models are available, for both P- and S-wave velocities. Model extenion is roughly 8° of latitude with 4° overlap between models. Grid cells are cubes of 4 km.

Velocity models are identified by their (rough) latitude range: for example, `18_26` indicates the model extanding from 18°S to (almost) 26°S.

Models are distributed in `NonLinLoc` format (Lomax, 2000 ; see http://alomax.free.fr/nlloc/ for details and usage).

